﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.IO;

using Innovista.RealTimeMessaging;

namespace AvailityMessageDestinationProvider
{
	public class AvailityCoreProvider : IMessageDestinationProvider
	{
		public AvailityCoreProvider()
		{
		}

		public RealTimeMessage FormatMessageBodyForSend(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
		{
			try
			{				
				oMessageToFormat.MessageBody = oMessageToFormat.RawMessageData;
				if (oMessageToFormat.RawMessageData == string.Empty)
				{
					oMessageToFormat.MessageBody = string.Empty;
					oMessageToFormat.ErrorStatus = true;
					oMessageToFormat.ErrorCode = RealTimeMessageGenericErrorCodes.NoRawMessageData;
				}

				return oMessageToFormat;
			}
			catch (Exception ex)
			{
				throw new Innovista.Common.Exceptions.InnovistaException("An unexpected error occurred.", ex);
			}
		}
		private string GetX12MessageWrapper(RealTimeMessage oMessage, CompanyMessageDestination compInfo, string runMode)
		{
			string sISAHeader= string.Empty;
			string sGSHeader= string.Empty;
			string sGEFooter = string.Empty;
			string sIEAFooter = string.Empty;
			DateTime dtNow = DateTime.UtcNow;
			string sDateNow = GetInterchangeDate(dtNow, 2);
			string sTimeNow = GetInterchangeTime(dtNow);

			//ISA*03*S269359  *01*fkehMAJIiK*ZZ*AV09311993     *01*030240928      *110301*2144*^*00501*000000002*0*T*:~
			//sISAHeader = String.Format("ISA*03*{0}*01*{1}*ZZ*AV09311993     *01*030240928      *{2}*{3}*^*00501*{4}*0*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, oMessage.EZCapAuthNumber, runMode);
			//sISAHeader = String.Format("ISA*03*{0}*01*{1}*ZZ*AV09311993     *01*030240928      *{2}*{3}*^*00501*{4}*0*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, oMessage.EZCapAuthNumber, runMode);
			sISAHeader = String.Format("ISA*03*{0}*01*{1}*ZZ*AV09311993     *01*593547616      *{2}*{3}*^*00501*{4}*0*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, oMessage.EZCapAuthNumber, runMode);
			sISAHeader = "ISA*03*S269359  *01*fkehMAJIiK*01*AV09311993     *ZZ*593547616      *191114*1014*^*00501*000000003*0*T*:";
			//ISA*00*          *00*          *01*OSHMI          *01*278TEST        *191112*0959*^*00501*000000017*1*T*:~
			//sISAHeader = String.Format("ISA*00*{0}*00*{1}*01*OSHMI          *01*278TEST        *{2}*{3}*^*00501*{4}*1*{5}*:", GetUserNamePlaceholder(), GetPasswordPlaceholder(), sDateNow, sTimeNow, "000000017", runMode);



			DateTime dtGSNow = DateTime.UtcNow;
			string sGSDateNow = GetInterchangeDate(dtGSNow, 4);
			string sGSTimeNow = GetInterchangeTime(dtGSNow);
			//GS*HI*AV01101957*030240928*20110301*214457*2*X*005010X217~		
			sGSHeader = String.Format("GS*HI*AV01101957*593547616*{0}*{1}*{2}*X*005010X217", sGSDateNow, sGSTimeNow, oMessage.EZCapAuthNumber);
			sGSHeader = "GS*HI*AV09311993*593547616*20191114*1014*3*X*005010X217~";
			//GS*HI*OSHMI*278TEST*20191112*0959*17*X*005010X217~
			//sGSHeader = String.Format("GS*HI*OSHMI*278TEST*{0}*{1}*{2}*X*005010X217", sGSDateNow, sGSTimeNow, "17");

			//sGEFooter = String.Format("GE*1*{0}", oMessage.TranTypeID);
			sGEFooter = String.Format("GE*1*{0}", oMessage.EZCapAuthNumber);
			sGEFooter = "GE*1*3";
			//sIEAFooter = String.Format("IEA*1*{0}", oMessage.EZCapAuthNumber);
			sIEAFooter = String.Format("IEA*1*{0}", oMessage.EZCapAuthNumber);
			return String.Format("{0}~{1}~{2}~{3}~{4}~", sISAHeader, sGSHeader, oMessage.RawMessageData, sGEFooter, sIEAFooter);
		}
		#region HelperMethods
		private string GetInterchangeDate(DateTime dtDate, int numChars)
		{
			string sNow = string.Empty;
			if (numChars <= 2)
				sNow = dtDate.Year.ToString().Substring(2, 2);
			else if (numChars >= 4)
				sNow = dtDate.Year.ToString();

			string sMonthPart = dtDate.Month.ToString();
			if (sMonthPart.Length == 1)
				sMonthPart = "0" + sMonthPart;
			sNow += sMonthPart;
			sMonthPart = dtDate.Day.ToString();
			if (sMonthPart.Length == 1)
				sMonthPart = "0" + sMonthPart;
			sNow += sMonthPart;
			return sNow;
		}
		private string GetInterchangeTime(DateTime dtDate)
		{
			string sNow = string.Empty;
			string sTimePart = dtDate.Hour.ToString();
			if (sTimePart.Length == 1)
				sTimePart = "0" + sTimePart;
			sNow += sTimePart;
			sTimePart = dtDate.Minute.ToString();
			if (sTimePart.Length == 1)
				sTimePart = "0" + sTimePart;
			sNow += sTimePart;
			return sNow;
		}
		private string GetTranCode(string EDIMessageType)
		{
			string retCode = String.Empty;
			if (EDIMessageType.ToUpper() == "EDI278")
				retCode = "278";

			return retCode;
		}
		private string GetTranMessageFormat()
		{
			return "X12";
		}
		private string GetUserNamePlaceholder()
		{
			return "[UserNameTBD]";
		}
		private string GetPasswordPlaceholder()
		{
			return "[PasswordTBD]";
		}
		private string GetAvailityReceiver()
		{
			return "AVAILITY";
		}
		protected virtual void SendRequest(HttpWebRequest request, string payload)
		{
			using (var writer = new StreamWriter(request.GetRequestStream(), Encoding.Default))
			{
				writer.Write(payload);
			}
		}
		protected virtual string GetResponse(HttpWebRequest request)
		{
			var response = (HttpWebResponse)request.GetResponse();
			using (var reader = new StreamReader(response.GetResponseStream()))
			{
				return reader.ReadToEnd();
			}
		}

		#endregion

		private System.ServiceModel.Channels.Binding GetAvailitySoapBinding()
		{
			System.ServiceModel.Channels.CustomBinding retBinding = new System.ServiceModel.Channels.CustomBinding();
			System.ServiceModel.Channels.TransportSecurityBindingElement securityElement = System.ServiceModel.Channels.TransportSecurityBindingElement.CreateUserNameOverTransportBindingElement();
			securityElement.IncludeTimestamp = false;
			retBinding.Elements.Add(securityElement);
			
			System.ServiceModel.Channels.TextMessageEncodingBindingElement msgEncoding = new System.ServiceModel.Channels.TextMessageEncodingBindingElement();
			msgEncoding.MessageVersion = System.ServiceModel.Channels.MessageVersion.Soap12WSAddressing10;
			retBinding.Elements.Add(msgEncoding);

			System.ServiceModel.Channels.BindingElementCollection elements = retBinding.CreateBindingElements();
			elements.Find<System.ServiceModel.Channels.TextMessageEncodingBindingElement>().MessageVersion = System.ServiceModel.Channels.MessageVersion.Soap12WSAddressing10;

			System.ServiceModel.Channels.HttpsTransportBindingElement httpsElement = new System.ServiceModel.Channels.HttpsTransportBindingElement();
			retBinding.Elements.Add(httpsElement);

			return retBinding;
		}

		public RealTimeMessage SendMessage(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
		{
			try
			{
				System.ServiceModel.Channels.Binding svcBinding = GetAvailitySoapBinding();
				Core.RealTimeTransactionResponse coreResponse = new Core.RealTimeTransactionResponse();

				//Core.CORETransactionsClient coreClient = new Core.CORETransactionsClient("CoreSoapPortCustom");
				Core.CORETransactionsClient coreClient = new Core.CORETransactionsClient(svcBinding, new System.ServiceModel.EndpointAddress(messageDestinationInfo.Endpoint));
				coreClient.ClientCredentials.UserName.UserName = messageDestinationInfo.UserName;
				coreClient.ClientCredentials.UserName.Password = messageDestinationInfo.Password;

				Core.COREEnvelopeRealTimeRequest coreRequest = new Core.COREEnvelopeRealTimeRequest();
				coreRequest.PayloadType = GetPayloadType(oMessageToFormat);
				coreRequest.ProcessingMode = "RealTime";
				coreRequest.PayloadID = oMessageToFormat.EZCapAuthNumber;
				coreRequest.TimeStamp = DateTime.Now.ToString("o");
				coreRequest.SenderID = messageDestinationInfo.UserName; //must match username in credential section above
				coreRequest.ReceiverID = "AVAILITY";
				coreRequest.CORERuleVersion = "2.2.0";
				//coreRequest.Payload = "ISA*03*          *01*          *ZZ*AV09311993     *01*030240928      *180813*1135*^*00501*837043319*0*T*:~GS*HI*V10570*030240928*20180813*113946*1234*X*005010X217~ST*278*1001*005010X217~BHT*0007*13*9999970636*20180629*1603~HL*1**20*1~NM1*X3*2*PAYER NAME*****PI*PAYERID~HL*2*1*21*1~NM1*FA*2*NAME*****XX*1234567893~N3*1234 MAIN ST~N4*ANYTOWN*TX*12345~PER*IC*ADMISSION NOTIFICATION REPORT LINE~HL*3*2*22*1~NM1*IL*1*LNAME TEST*FNAME TEST*X***MI*123456789~REF*6P*10321068~DMG*D8*19990105*F~HL*4*3*EV*0~TRN*1*7673470636*3030240928*7673470636~UM*AR*I*1*11:A**03~DTP*435*D8*20180629~HI*ABJ:R42~CL1*2*1~MSG*AT=150200;;~NM1*FA*2*NAME*****XX*1234567893~N3*1234 MAIN ST~N4*ANYTOWN*TX*12345~PER*IC*ADMISSION NOTIFICATION REPORT LINE*TE*1231231234~NM1*71*1*LNAME*FNAME*NAME***XX*1234067894~N3*4321 ADDRESS CIRCLE~N4*ANYTOWN*TX*12345~PER*IC**TE*3213214321*FX*3213211234~PRV*AT*PXC*207Q00000X~SE*30*1001~GE*1*1234~IEA*1*837043319~";
				coreRequest.Payload = oMessageToFormat.MessageBody;

				coreResponse = coreClient.RealTimeTransactionAsync(coreRequest).Result;
				oMessageToFormat.MessageResponse = coreResponse.ToString();
				return oMessageToFormat;
			}
			catch (Exception ex)
			{
				throw new Innovista.Common.Exceptions.InnovistaException("An unexpected error occurred.", ex);
			}
		}
		private string GetPayloadType(RealTimeMessage oMessageToSend)
		{
			switch (oMessageToSend.EDIMessageType.ToUpper())
			{
				case "EDI278":
					return "X12_278_Request_005010X217";
				default:
					return string.Empty;
			}
		}		

		public string GetProviderRunMode(string runMode)
		{
			string sRetVal = string.Empty;
			if (runMode == ProcessRunMode.Test)
				sRetVal = "T";
			else if (runMode == ProcessRunMode.Production)
				sRetVal = "P";
			return sRetVal;
		}
	}
}
