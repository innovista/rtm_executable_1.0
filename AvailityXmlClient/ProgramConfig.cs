﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AvailityXmlClient
{
	public static class ProgramConfig
	{
		private readonly static string appConnection;

		static ProgramConfig()
		{
			appConnection = IsDefined(ConfigurationManager.AppSettings["AppSQLConn"]);
		}
		public static string AppConnection
		{
			get { return appConnection; }
		}

		#region HelperFunctions
		private static bool FalseIfBlank(string strInput)
		{
			bool retVal = false;

			try
			{
				retVal = bool.Parse(strInput);
			}
			catch
			{
			}
			return retVal;

		}
		private static string IsDefined(string appsetting)
		{
			return IsDefined(appsetting, "");
		}
		private static string IsDefined(string appsetting, string defaultValue)
		{
			string retVal = defaultValue;

			try
			{
				if (appsetting != null)
					retVal = appsetting;
			}
			catch
			{
			}
			return retVal;
		}
		private static int IsDefined(string strInput, int defaultValue)
		{
			int retVal = defaultValue;

			try
			{
				retVal = int.Parse(strInput);
			}
			catch
			{
			}
			return retVal;
		}

		#endregion
	}
}
