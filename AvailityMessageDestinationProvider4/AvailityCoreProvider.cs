﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.IO;
using System.ServiceModel;

using Innovista.RealTimeMessaging;

namespace AvailityMessageDestinationProvider
{    
    public class AvailityCoreProvider : IMessageDestinationProvider
	{
        private const string RESPONSE_HEADER_2000A = "HL*1**20*1";
        private const string RESPONSE_HEADER_2000B = "HL*2*1*21*1";
        private const string RESPONSE_HEADER_2000E = "HL*4*3*EV*1";
        private const string RESPONSE_HEADER_2000F = "HL*5*4*SS*0";

        public AvailityCoreProvider()
		{
		}
        /// <summary>
        /// Format the realtime message and prepare it to be sent. Primarily applicable when not using a class-based approach and built in framework to "build"
        /// </summary>
        /// <param name="oMessageToFormat"></param>
        /// <param name="messageDestinationInfo"></param>
        /// <param name="runMode"></param>
        /// <returns></returns>
		public RealTimeMessage FormatMessageBodyForSend(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
		{
			try
			{				
				oMessageToFormat.MessageBody = oMessageToFormat.RawMessageData;
				if (oMessageToFormat.RawMessageData == string.Empty)
				{
					oMessageToFormat.MessageBody = string.Empty;
					oMessageToFormat.ErrorStatus = true;
					oMessageToFormat.ErrorCode = RealTimeMessageGenericErrorCodes.NoRawMessageData;
				}

				return oMessageToFormat;
			}
			catch (Exception ex)
			{
                oMessageToFormat.ErrorStatus = true;
                oMessageToFormat.ErrorCode = RealTimeMessageGenericErrorCodes.NoRawMessageData;
                oMessageToFormat.ErrorMessage = ex.Message;
                throw new Innovista.Common.Exceptions.InnovistaException("An unexpected error occurred.", ex);
			}
		}
        #region HelperMethods
        /// <summary>
        /// Returns the correct Availity Payload Type based upon the EDI message being sent.
        /// </summary>
        /// <param name="oMessageToSend">RealTimeMessage containing the EDI message type</param>
        /// <returns></returns>
        private string GetPayloadType(RealTimeMessage oMessageToSend)
        {
            switch (oMessageToSend.EDIMessageType.ToUpper())
            {
                case "EDI278":
                    return "X12_278_Request_005010X217";
                default:
                    return string.Empty;
            }
        }
        private string GetAvailityReceiver()
        {
            //return "AVAILITY";
            return string.Empty;
        }

        #endregion

        /// <summary>
        /// Build the WCF binding required to call the Availity SOAP+XML Core service
        /// </summary>
        /// <returns></returns>
        private System.ServiceModel.Channels.Binding GetAvailitySoapBinding()
		{
            System.ServiceModel.Channels.CustomBinding retBinding = new System.ServiceModel.Channels.CustomBinding();
            System.ServiceModel.Channels.TransportSecurityBindingElement securityElement = System.ServiceModel.Channels.TransportSecurityBindingElement.CreateUserNameOverTransportBindingElement();
            securityElement.IncludeTimestamp = false;            
            retBinding.Elements.Add(securityElement);

            System.ServiceModel.Channels.TextMessageEncodingBindingElement msgEncoding = new System.ServiceModel.Channels.TextMessageEncodingBindingElement(System.ServiceModel.Channels.MessageVersion.Soap12, Encoding.UTF8);
            retBinding.Elements.Add(msgEncoding);

            System.ServiceModel.Channels.HttpsTransportBindingElement httpsElement = new System.ServiceModel.Channels.HttpsTransportBindingElement();
            retBinding.Elements.Add(httpsElement);
            return retBinding;
        }
        /// <summary>
        /// Send the message to the provider and process the initial response.
        /// </summary>
        /// <param name="oMessageToFormat">Contains all the relevant information regarding the EDI message to be sent.</param>
        /// <param name="messageDestinationInfo">Contains the information regarding the destination of where the message is to be sent</param>
        /// <param name="runMode">Indicates if we're running in test or production</param>
        /// <returns></returns>
		public RealTimeMessage SendMessage(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
		{
			try
			{
				System.ServiceModel.Channels.Binding svcBinding = GetAvailitySoapBinding();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                Core.COREEnvelopeRealTimeResponse coreResponse = new Core.COREEnvelopeRealTimeResponse();
                //using (Core.CORETransactionsClient coreClient = new Core.CORETransactionsClient("CoreSoapPortCustom"))
                using (Core.CORETransactionsClient coreClient = new Core.CORETransactionsClient(svcBinding, new System.ServiceModel.EndpointAddress(messageDestinationInfo.Endpoint)))
                {
					coreClient.ClientCredentials.UserName.UserName = messageDestinationInfo.UserName; //"S269359";     //messageDestinationInfo.UserName;
					coreClient.ClientCredentials.UserName.Password = messageDestinationInfo.Password; //"fkehMAJIiK";  //messageDestinationInfo.Password;

                    Core.COREEnvelopeRealTimeRequest coreRequest = new Core.COREEnvelopeRealTimeRequest();
					coreRequest.PayloadType = GetPayloadType(oMessageToFormat);
					coreRequest.ProcessingMode = "RealTime";
					coreRequest.PayloadID = oMessageToFormat.EZCapAuthNumber;
					coreRequest.TimeStamp = DateTime.Now.ToString("o");
					coreRequest.SenderID = messageDestinationInfo.UserName; //must match username in credential section above
					coreRequest.ReceiverID = GetAvailityReceiver();
					coreRequest.CORERuleVersion = "2.2.0";
                    //coreRequest.Payload = "ISA*03*          *01*          *ZZ*AV09311993     *01*030240928      *180813*1135*^*00501*837043319*0*T*:~GS*HI*V10570*030240928*20180813*113946*1234*X*005010X217~ST*278*1001*005010X217~BHT*0007*13*9999970636*20180629*1603~HL*1**20*1~NM1*X3*2*PAYER NAME*****PI*PAYERID~HL*2*1*21*1~NM1*FA*2*NAME*****XX*1234567893~N3*1234 MAIN ST~N4*ANYTOWN*TX*12345~PER*IC*ADMISSION NOTIFICATION REPORT LINE~HL*3*2*22*1~NM1*IL*1*LNAME TEST*FNAME TEST*X***MI*123456789~REF*6P*10321068~DMG*D8*19990105*F~HL*4*3*EV*0~TRN*1*7673470636*3030240928*7673470636~UM*AR*I*1*11:A**03~DTP*435*D8*20180629~HI*ABJ:R42~CL1*2*1~MSG*AT=150200;;~NM1*FA*2*NAME*****XX*1234567893~N3*1234 MAIN ST~N4*ANYTOWN*TX*12345~PER*IC*ADMISSION NOTIFICATION REPORT LINE*TE*1231231234~NM1*71*1*LNAME*FNAME*NAME***XX*1234067894~N3*4321 ADDRESS CIRCLE~N4*ANYTOWN*TX*12345~PER*IC**TE*3213214321*FX*3213211234~PRV*AT*PXC*207Q00000X~SE*30*1001~GE*1*1234~IEA*1*837043319~";
                    coreRequest.Payload = oMessageToFormat.MessageBody;
                    //coreRequest.Payload = String.Format("<![CDATA[{0}]]>", oMessageToFormat.MessageBody);
                    coreResponse = coreClient.RealTimeTransaction(coreRequest);
                    if (coreResponse.ErrorCode.Trim().ToLower() != "success")
                    {
                        oMessageToFormat.ErrorStatus = true;
                        oMessageToFormat.ErrorCode = coreResponse.ErrorCode;
                        oMessageToFormat.MessageResponse = coreResponse.ErrorMessage;
                        oMessageToFormat.ErrorMessage = coreResponse.ErrorMessage;
                        return oMessageToFormat;
                    }
                    oMessageToFormat.MessageResponse = coreResponse.Payload;
				}
				//oMessageToFormat.MessageResponse = coreResponse.ToString();
				return oMessageToFormat;
			}
			catch (Exception ex)
			{
                oMessageToFormat.ErrorStatus = true;
                oMessageToFormat.ErrorCode = RealTimeMessageGenericErrorCodes.UnhandledError;
                oMessageToFormat.ErrorMessage = ex.Message;
                oMessageToFormat.MessageResponse = ex.Message;
				throw new Innovista.Common.Exceptions.InnovistaException("An unexpected error occurred.", ex);
			}
		}        

        /// <summary>
        /// Takes in a ProcessRunMode string and converts it to an equivalent for this provider
        /// </summary>
        /// <param name="runMode"></param>
        /// <returns></returns>
		public string GetProviderRunMode(string runMode)
		{
			string sRetVal = string.Empty;
			if (runMode == ProcessRunMode.Test)
				sRetVal = "T";
			else if (runMode == ProcessRunMode.Production)
				sRetVal = "P";
			return sRetVal;
		}

        private RealTimeMessage GetWellcareResponse(RealTimeMessage rtMessage)
        {
            string authNumber = string.Empty;
            bool bIn200E = false;
            bool bIn200A = false;
            string sHCRSegment = string.Empty;
            string sBBSegment = string.Empty;
            string sAAASegment = string.Empty;
            string sMSGSegment = string.Empty;

            //remove any newline information, if exists as it may cause issue with parsing
            string messageResponse = rtMessage.MessageResponse.Replace(Environment.NewLine, "");
            string[] arrResponseLines = messageResponse.Split('~');

            foreach(string responseLine in arrResponseLines)
            {
                //tracking which segment of response we're in
                if ((responseLine.ToUpper() == RESPONSE_HEADER_2000E) && (!bIn200E))
                    bIn200E = true;

                if ((responseLine.ToUpper() == RESPONSE_HEADER_2000F) && (bIn200E))
                    bIn200E = false;

                if ((responseLine.ToUpper() == RESPONSE_HEADER_2000A) && (!bIn200A))
                    bIn200A = true;
                if ((responseLine.ToUpper() == RESPONSE_HEADER_2000B) && (bIn200A))
                    bIn200A = false;

                if (bIn200A)
                {
                    string[] sLineSegment = responseLine.Split('*');
                    string sSegmentHeader = sLineSegment[0].ToUpper();
                    switch (sSegmentHeader)
                    {
                        case "AAA":
                            sAAASegment = responseLine;
                            break;
                        case "MSG":
                            sMSGSegment = responseLine;
                            break;
                    }
                }
                if (bIn200E)
                {
                    string[] sLineSegment = responseLine.Split('*');
                    string sSegmentHeader = sLineSegment[0].ToUpper();
                    switch (sSegmentHeader)
                    {
                        case "HCR":
                            sHCRSegment = responseLine;
                            break;
                        case "REF":
                            if (sLineSegment[1].ToUpper() == "BB")
                                sBBSegment = responseLine;
                            break;
                        case "AAA":
                            sAAASegment = responseLine;
                            break;
                        case "MSG":
                            sMSGSegment = responseLine;
                            break;
                    }
                }
            }

            //process errors if AAA segment is returned
            if (!String.IsNullOrEmpty(sAAASegment))
            {
                string[] sAAASegs = sAAASegment.Split('*');
                string sRejectCode = sAAASegs[3];
                rtMessage = HandleAAASegmentErrors(sRejectCode, sAAASegment, sMSGSegment, rtMessage);
            }

            //return auth number if provided in HCR Segment
            if (!String.IsNullOrEmpty(sHCRSegment))
            {
                string[] sSegmentArgs = sHCRSegment.Split('*');
                switch (sSegmentArgs[1].ToUpper())
                {
                    case "A1":
                        authNumber = sSegmentArgs[2];
                        break;
                    case "A3":
                    case "C":
                        rtMessage.ErrorStatus = false;
                        rtMessage.ErrorCode = string.Empty;
                        string[] sBBSegArgs = sBBSegment.Split('*');
                        authNumber = sBBSegArgs[2];
                        break;
                    case "A4":
                        //request out of network / denied
                        rtMessage.ErrorStatus = true;
                        rtMessage.ErrorCode = WellcareErrorCodes.WC_999_RequestNotApproved;
                        rtMessage.ErrorMessage = "Request out of network / Recommended for Denial.";
                        break;
                }
            }

            
            rtMessage.HPAuthNumber = authNumber;
            return rtMessage;
        }

        private RealTimeMessage HandleAAASegmentErrors(string sRejectCode, string sAAASegment, string sMsgSegment, RealTimeMessage rtMessage)
        {
            //set error status and code. will provide message unique to each code
            rtMessage.ErrorStatus = true;
            rtMessage.ErrorCode = sRejectCode;

            switch (sRejectCode)
            {
                case WellcareErrorCodes.WC_15_RequiredDataMessing:
                    rtMessage.ErrorMessage = "Required data missing";
                    break;
                case WellcareErrorCodes.WC_33_InputErrors:
                    rtMessage.ErrorMessage = "Input Error";
                    break;
                case WellcareErrorCodes.WC_42_NoResponse:
                    rtMessage.ErrorMessage = "Timeout from CareCentral";
                    break;
                case WellcareErrorCodes.WC_43_InvalidProviderIdent:
                    rtMessage.ErrorMessage = "Invalid Provider Identification";
                    break;
                case WellcareErrorCodes.WC_44_InvalidProviderName:
                    rtMessage.ErrorMessage = "Invalid Provider Name";
                    break;
                case WellcareErrorCodes.WC_51_ProviderNotOnFile:
                    rtMessage.ErrorMessage = "Provider Not On File";
                    break;
                case WellcareErrorCodes.WC_57_InvalidDatesOfService:
                    rtMessage.ErrorMessage = "Invalid Dates of Service";
                    break;
                case WellcareErrorCodes.WC_AF_InvalidDiagnosisCode:
                    rtMessage.ErrorMessage = "Invalid Diagnosis Code";
                    break;
                case WellcareErrorCodes.WC_AM_InvalidAdmissionDate:
                    rtMessage.ErrorMessage = "Invalid Admission Date";
                    break;
                case WellcareErrorCodes.WC_T4_PayerMissing:
                    rtMessage.ErrorMessage = "Payer Name / ID Missing";
                    break;
                case "":
                    rtMessage.ErrorStatus = false;
                    rtMessage.ErrorCode = string.Empty;
                    break;
                default:
                    break;
            }

            if (!String.IsNullOrEmpty(sMsgSegment))
                rtMessage.ErrorMessage = string.Format("{0} ({1}) - {2}", rtMessage.ErrorMessage, rtMessage.ErrorCode, sMsgSegment);

            return rtMessage;
        }
        /// <summary>
        /// Based upon the Wellcare Response to 278 requests, this method returns the WellCare Auth Number if it was provided in a 
        /// successful response back from Availity. The logic is based explicitly on a spec provided by WellCare and is unique
        /// to this destination provider.
        /// </summary>
        /// <param name="MessageResponse"></param>
        /// <returns>WellCare Auth Number, if available</returns>
        private string GetWellCareAuthNumberFromResponse(string MessageResponse)
        {
            string sAuthNumber = string.Empty;
            //look for starting point where auth number section is:  starts with HL*4*3*EV*1
            string sStartMatchString = "HL*4*3*EV*1";
            string sEndMatchString = "HL*5*4*SS*0";
            string sResponseExistsMatchString = "HCR*";
            string sCandidateString = string.Empty;

            int iStartIdx = MessageResponse.IndexOf(sStartMatchString);
            if (iStartIdx>=0)
            {
                iStartIdx += sStartMatchString.Length;
                int iHasResponse = MessageResponse.IndexOf(sResponseExistsMatchString, iStartIdx);
                if (iHasResponse >= 0)
                {
                    int iEndIndex = MessageResponse.IndexOf(sEndMatchString);
                    if (iEndIndex >= 0)
                    {
                        sCandidateString = MessageResponse.Substring(iHasResponse, (iEndIndex - iHasResponse));
                        string sResponseAction = string.Empty;
                        if (sCandidateString.Length > sResponseExistsMatchString.Length+2)
                        {
                            sResponseAction = sCandidateString.Substring((sResponseExistsMatchString.Length), 2);
                        }
                        switch (sResponseAction.ToUpper())
                        {
                            case "A1":
                                //initial response
                                int iTempIndexAuth = sCandidateString.IndexOf("~");
                                if (iTempIndexAuth >= 0)
                                {
                                    string sTempAuthNumber = sCandidateString.Substring((sResponseExistsMatchString.Length + 3), (iTempIndexAuth - (sResponseExistsMatchString.Length + 3)));
                                    string sBBString = ParseBBAuthNumber(sCandidateString);
                                    if (!string.IsNullOrEmpty(sBBString))
                                        sAuthNumber = sBBString;
                                    else
                                        sAuthNumber = sTempAuthNumber;
                                }
                                break;
                            case "A3:":
                                //duplicate response
                                //initial response
                                int iTempIndexDupAuth = sCandidateString.IndexOf("~");
                                if (iTempIndexDupAuth >= 0)
                                {
                                    string sBBString = ParseBBAuthNumber(sCandidateString);
                                    if (!string.IsNullOrEmpty(sBBString))
                                        sAuthNumber = sBBString;
                                }
                                break;
                            default:
                                break;
                        }                       
                    }
                }
            }
            //sAuthNumber = "987654321";
            return sAuthNumber;
        }
        /// <summary>
        /// Helper Method to retrieve a value if REF*BB* exists in the input string
        /// </summary>
        /// <param name="sInputString"></param>
        /// <returns>number if REF*BB* string exists</returns>
        private string ParseBBAuthNumber(string sInputString)
        {
            string sBBMatchString = "REF*BB*";
            string sRetVal = string.Empty;

            int iBBIndex = sInputString.IndexOf(sBBMatchString);
            if (iBBIndex >= 0)
            {
                string sBBString = sInputString.Substring(iBBIndex, (sInputString.Length - iBBIndex));
                iBBIndex = sBBString.IndexOf("~");
                if (iBBIndex >= 0)
                {
                    sBBString = sBBString.Substring(sBBMatchString.Length, iBBIndex - sBBMatchString.Length);
                    sRetVal = sBBString;
                }
            }
            return sRetVal;
        }
        /// <summary>
        /// Handles logic for retrieving and populating desired response values out of the message response, as well as
        /// handling certain error conditions that can be handled and subsequently "requeued" for processing later
        /// </summary>
        /// <param name="oMessageToFormat">RealTimeMessage object containing the response to process</param>
        /// <param name="messageDestinationInfo">object containing information about message destination, if required</param>
        /// <param name="runMode">string determining if we're running in test or production</param>
        /// <returns></returns>
        public RealTimeMessage ProcessResponse(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
        {
            try
            {
                if (oMessageToFormat.ErrorStatus == false)
                {
                    ////we're handling a successfully sent message so we need to process the payload
                    //string authNumber = GetWellCareAuthNumberFromResponse(oMessageToFormat.MessageResponse);
                    //if (authNumber != string.Empty)
                    //{
                    //    oMessageToFormat.HPAuthNumber = authNumber;
                    //    return oMessageToFormat;
                    //} else
                    //{
                    //    //unable to retrieve auth number so handle accordingly
                    //    oMessageToFormat.ErrorStatus = true;
                    //    oMessageToFormat.ErrorCode = RealTimeMessageGenericErrorCodes.AuthNumberNotIncluded;
                    //}
                    oMessageToFormat = GetWellcareResponse(oMessageToFormat);
                    if (!String.IsNullOrEmpty(oMessageToFormat.HPAuthNumber))
                        return oMessageToFormat;
                    else
                    {
                        if (!oMessageToFormat.ErrorStatus)
                        {
                            oMessageToFormat.ErrorStatus = true;
                            oMessageToFormat.ErrorCode = RealTimeMessageGenericErrorCodes.AuthNumberNotIncluded;
                            oMessageToFormat.ErrorMessage = "Auth number not found in response.";
                        }
                    }
                } else
                {
                    //we're handling error conditions so we need to review error codes and determine next steps
                    switch(oMessageToFormat.ErrorCode)
                    {
                        case RealTimeMessageGenericErrorCodes.UnhandledError:
                            break;                        
                        case AvailityErrorCodes.VT07_037_RetryLater:
                        case AvailityErrorCodes.VT05_035_UnableToConnect:
                        case AvailityErrorCodes.VT02_030_MediatorPoolFull:
                        case AvailityErrorCodes.VT02_029_VendorPoolFull:
                            //message wasn't received all the way through so we need to resend.  Reset values accordingly
                            oMessageToFormat.ProcessStatus = ProcessStatus.ReadyForMessageSending;
                            oMessageToFormat.ErrorStatus = false;
                            oMessageToFormat.ErrorCode = string.Empty;
                            break;
                        default:
                            break;
                    }
                }                
                return oMessageToFormat;
            }
            catch (Exception ex)
            {
                throw new Innovista.Common.Exceptions.InnovistaException("An unexpected error occurred.", ex);
            }
        }
        /// <summary>
        /// Responsible for trying to re-process provider-specific error messages
        /// </summary>
        /// <param name="oMessageToFormat">RealTimeMessage object containing the response to process</param>
        /// <param name="messageDestinationInfo">object containing information about message destination, if required</param>
        /// <param name="runMode">string determining if we're running in test or production</param>
        /// <returns>Updated RealTimeMessage object with any changes related to handling errors</returns>
        public RealTimeMessage ReprocessHandledError(RealTimeMessage oMessageToFormat, CompanyMessageDestination messageDestinationInfo, string runMode)
        {
            if (oMessageToFormat.ErrorStatus == true)
            {
                //timeout error so we can reset for next time
                if (oMessageToFormat.ErrorCode == WellcareErrorCodes.WC_42_NoResponse)
                {                    
                    oMessageToFormat.ErrorStatus = false;
                    oMessageToFormat.ErrorCode = string.Empty;
                    oMessageToFormat.ProcessStatus = "MSG_BODY_GEN_SUCCESS";
                }
            }
            return oMessageToFormat;
        }
    }
}
