﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovista.RealTimeMessaging
{
	public sealed class RealTimeMessageGenericErrorCodes
	{
		public static string UnhandledError = "INV9999";
		
		public static string NoRawMessageData = "INV0001";
		public static string GeneralApplicationResponseError = "INV0002";
	}
}
