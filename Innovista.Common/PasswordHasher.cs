﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCrypt.Net;

namespace Innovista.Common
{
	
	public class PasswordHasher
	{
		private static int SALT_COMPLEXITY = 10;
		private string _salt { get; set; }
		public PasswordHasher()
		{
			_salt = BCrypt.Net.BCrypt.GenerateSalt(SALT_COMPLEXITY);
		}
		public string HashPassword(string inputPassword)
		{
			return BCrypt.Net.BCrypt.HashPassword(inputPassword, _salt);
		}
		public bool CheckPassword(string passwordToValidate, string storedPassword)
		{
			return BCrypt.Net.BCrypt.CheckPassword(passwordToValidate, storedPassword);
		}
	}
}
