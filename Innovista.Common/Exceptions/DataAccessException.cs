﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Innovista.Common.Exceptions
{
	/// <summary>
	/// Exception for invalid input, typically on client-side
	/// </summary>
	[Serializable]
	public class DataAccessException : InnovistaException
	{
		public DataAccessException()
			: base()
		{
		}
		public DataAccessException(string Message)
			: base(Message)
		{
		}
		public DataAccessException(string Message, Exception inner)
			: base(Message, inner)
		{
		}
		public DataAccessException(SerializationInfo Info, StreamingContext context)
			: base(Info, context)
		{
		}
	}
}
