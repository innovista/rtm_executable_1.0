﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Innovista.Common.Exceptions
{
	/// <summary>
	/// Exception from provider for well-known error conditions
	/// </summary>
	[Serializable]
	public class ProviderKnownException : InnovistaException
	{
		public ProviderKnownException()
			: base()
		{
		}
		public ProviderKnownException(string Message)
			: base(Message)
		{
		}
		public ProviderKnownException(string Message, Exception inner)
			: base(Message, inner)
		{
		}
		public ProviderKnownException(SerializationInfo Info, StreamingContext context)
			: base(Info, context)
		{
		}
	}
}
