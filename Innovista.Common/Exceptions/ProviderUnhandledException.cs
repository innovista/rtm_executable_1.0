﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Innovista.Common.Exceptions
{
	/// <summary>
	/// Exception from provider for well-known error conditions
	/// </summary>
	[Serializable]
	public class ProviderUnhandledException : InnovistaException
	{
		public ProviderUnhandledException()
			: base()
		{
		}
		public ProviderUnhandledException(string Message)
			: base(Message)
		{
		}
		public ProviderUnhandledException(string Message, Exception inner)
			: base(Message, inner)
		{
		}
		public ProviderUnhandledException(SerializationInfo Info, StreamingContext context)
			: base(Info, context)
		{
		}
	}
}
