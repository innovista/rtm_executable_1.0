﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Innovista.Common;
using Innovista.RealTimeMessaging.Data;
using System.Data;

namespace Innovista.RealTimeMessaging.Engine
{
	public enum SupportedProviderEnum
	{
		Availity = 1,
		AvailityCore = 2
	}
	public class RealTimeMessageProcessing
	{
		private ProgramOptions curOptions { get; set; }
		public RealTimeMessageProcessing(ProgramOptions runOptions)
		{
			curOptions = runOptions;

			if (curOptions.RunMode == null)
				curOptions.RunMode = ProcessRunMode.Test;
		}
		public void Process()
		{
			if (curOptions.inputProcessStatus == ProcessStatus.ReadyForMessageFormatting)
			{
				ProcessRealTimeMessagingStep2();
			}
			if (curOptions.inputProcessStatus == ProcessStatus.ReadyForMessageSending)
			{
				ProcessRealTimeMessagingStep3();
			}
		}
		private IMessageDestinationProvider GetProvider(int MessageDestinationID)
		{
			IMessageDestinationProvider oProvider = null;
			switch (MessageDestinationID)
			{
				case (int)SupportedProviderEnum.Availity:
					oProvider = new AvailityMessageDestinationProvider.AvailityProvider();
					break;
				case (int)SupportedProviderEnum.AvailityCore:
					oProvider = new AvailityMessageDestinationProvider.AvailityCoreProvider();
					break;
			}
			return oProvider;
		}
		private void ProcessRealTimeMessagingStep3()
		{
			RealTimeMessagingDataService objDataService = new RealTimeMessagingDataService(curOptions.ConnectionString);
			IMessageDestinationProvider oProvider = null;

			List<RealTimeMessage> MessagesToProcess = objDataService.GetMessagesToProcess(curOptions.inputProcessStatus);
			if (MessagesToProcess != null)
			{
				foreach (RealTimeMessage rtMessage in MessagesToProcess)
				{
					oProvider = GetProvider(rtMessage.MessageDestID);
					if (oProvider != null)
					{
						rtMessage.ProcessStatusStart = DateTime.UtcNow;
						objDataService.Update(rtMessage);

						CompanyMessageDestination messageDestinationInfo = objDataService.GetCompanyMessageDestination(rtMessage);

						oProvider.SendMessage(rtMessage, messageDestinationInfo, oProvider.GetProviderRunMode(curOptions.RunMode));
						if (!rtMessage.ErrorStatus)
						{
							rtMessage.ProcessStatus = ProcessStatus.MessageSentSuccessfully;
						}
						rtMessage.LastStepProcessEnd = DateTime.UtcNow;
						objDataService.Update(rtMessage);
					}
				}
			}

		}
		private void ProcessRealTimeMessagingStep2()
		{
			RealTimeMessagingDataService objDataService = new RealTimeMessagingDataService(curOptions.ConnectionString);
			IMessageDestinationProvider oProvider = null;

			List<RealTimeMessage> MessagesToProcess = objDataService.GetMessageBodyRequestsToBuild(curOptions.inputProcessStatus);
			if (MessagesToProcess != null)
			{
				foreach (RealTimeMessage rtMessage in MessagesToProcess)
				{
					oProvider = GetProvider(rtMessage.MessageDestID);
					if (oProvider != null)
					{
						rtMessage.ProcessStatusStart = DateTime.UtcNow;
						objDataService.Update(rtMessage);
						CompanyMessageDestination messageDestinationInfo = objDataService.GetCompanyMessageDestination(rtMessage);

						RealTimeMessage updatedMsg = oProvider.FormatMessageBodyForSend(rtMessage, messageDestinationInfo, oProvider.GetProviderRunMode(curOptions.RunMode));
						if (!updatedMsg.ErrorStatus)
						{
							updatedMsg.ProcessStatus = ProcessStatus.MessageFormattingSuccess;
						}
						updatedMsg.LastStepProcessEnd = DateTime.UtcNow;
						objDataService.Update(updatedMsg);
					}
				}
			}
		}

	}
}
