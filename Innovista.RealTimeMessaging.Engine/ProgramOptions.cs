﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;

namespace Innovista.RealTimeMessaging.Engine
{
	public class ProgramOptions
	{
		[Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
		public bool Verbose { get; set; }

		[Option('d', "cdevid", Required = false, HelpText = "cDevInstance ID.")]
		public string cDevInstanceID { get; set; }
		[Option('p', "inProcessStatus", Required = true, HelpText = "Input Process Status String of records to retrieve.")]
		public string inputProcessStatus { get; set; }
		[Option('c', "connectionString", Required = false, HelpText = "Database connection string to process.")]
		public string ConnectionString { get; set; }
		[Option('r', "runmode", Required = false, HelpText = "Determine if we're running in Test mode (T), or Prod mode (P).")]
		public string RunMode { get; set; }
	}
}
