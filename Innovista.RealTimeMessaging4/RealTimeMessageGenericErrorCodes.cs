﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovista.RealTimeMessaging
{
	public sealed class RealTimeMessageGenericErrorCodes
	{
		public const string UnhandledError = "INV9999";
		
		public const string NoRawMessageData = "INV0001";
		public const string GeneralApplicationResponseError = "INV0002";
        public const string AuthNumberNotIncluded = "INV0003";
        public const string DataAccessError = "INV0004";
    }
}
